package com.example.dbapp

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.CLS_ID
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.CLS_NAME
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.DATABASE_NAME
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.DATABASE_VERSION
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.STUD_CITY
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.STUD_CLASS
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.STUD_ID
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.STUD_NAME
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.STUD_PHONE
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.SUB_ID
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.SUB_NAME
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TABLE_NAME1
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TABLE_NAME2
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TABLE_NAME3
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TABLE_NAME4
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_ID
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_NAME
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_PHONE
import com.example.dbapp.AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_SUB

//class AcademyDbHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
class AcademyDbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    private var db: SQLiteDatabase ?= null

    override fun onCreate(db: SQLiteDatabase) {
        val createStudTable = ("CREATE TABLE $TABLE_NAME1($STUD_ID INTEGER PRIMARY KEY AUTOINCREMENT, $STUD_NAME TEXT, $STUD_PHONE INTEGER, $STUD_CITY TEXT, $STUD_CLASS INTEGER)")
        val createSubjectTable = ("CREATE TABLE $TABLE_NAME2($SUB_ID INTEGER PRIMARY KEY AUTOINCREMENT, $SUB_NAME TEXT)")
        val createClassTable = ("CREATE TABLE $TABLE_NAME3($CLS_ID INTEGER PRIMARY KEY AUTOINCREMENT, $CLS_NAME TEXT)")
        val createTeacherTable = ("CREATE TABLE $TABLE_NAME4($TEACHER_ID INTEGER PRIMARY KEY AUTOINCREMENT, $TEACHER_NAME TEXT, $TEACHER_PHONE INTEGER, $TEACHER_SUB INTEGER)")
        db.execSQL(createStudTable)
        db.execSQL(createSubjectTable)
        db.execSQL(createClassTable)
        db.execSQL(createTeacherTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME1")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME2")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME3")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME4")
        onCreate(db)
    }

    fun openDB() {
        db = this.writableDatabase
    }

    fun closeDB() {
        if (db?.isOpen == true) {
            db?.close()
        }
    }

    fun addStudent(studName: String, phone: String, city: String, classId: Int) {
        val values = ContentValues()
        values.put(STUD_NAME, studName)
        values.put(STUD_PHONE, phone)
        values.put(STUD_CITY, city)
        values.put(STUD_CLASS, classId)

        db?.insert(TABLE_NAME1, null, values)
    }

    fun addSubject(subName: String){
        val values = ContentValues()
        values.put(SUB_NAME,subName)
        db?.insert(TABLE_NAME2,null, values)
    }

    fun addClass(clsName: String){
        val values = ContentValues()
        values.put(CLS_NAME,clsName)

        db?.insert(TABLE_NAME3,null, values)
    }

    fun addTeacher(tchrName: String, phone: String, subId: Int){
        val values = ContentValues()
        values.put(TEACHER_NAME, tchrName)
        values.put(TEACHER_PHONE, phone)
        values.put(TEACHER_SUB, subId)
        db?.insert(TABLE_NAME4,null, values)
    }

    fun getAllStudent(clsId: Int): Cursor? {
        return db?.rawQuery("SELECT * FROM $TABLE_NAME1 WHERE $STUD_CLASS = $clsId", null)
    }

    fun getAllStudent(): Cursor? {
        return db?.rawQuery("SELECT * FROM $TABLE_NAME1", null)
    }

    fun getAllSubject(): Cursor? {
        return db?.rawQuery("SELECT * FROM $TABLE_NAME2", null)
    }

    fun getAllClass(): Cursor? {
        return db?.rawQuery("SELECT * FROM $TABLE_NAME3", null)
    }

    fun getAllTeacher(): Cursor? {
        return db?.rawQuery("SELECT * FROM $TABLE_NAME4", null)
    }

    fun getAllTeacher(subId: Int): Cursor? {
        return db?.rawQuery("SELECT * FROM $TABLE_NAME4 WHERE $TEACHER_SUB = $subId", null)
    }

    fun updateClass(classId:Int, updatedClassName: String) {
        val values = ContentValues()
        values.put(CLS_NAME,updatedClassName)
        db?.update(TABLE_NAME3,values, "_id=$classId", null )
    }

    fun updateStudent(studId:Int?, updatedName: String, updatedPhone: String, updatedCity: String, updatedClassId: Int) {
        val values = ContentValues()
        values.put(STUD_NAME, updatedName)
        values.put(STUD_PHONE, updatedPhone)
        values.put(STUD_CITY, updatedCity)
        values.put(STUD_CLASS, updatedClassId)
        db?.update(TABLE_NAME1,values, "_id=$studId", null )
    }

    fun updateSubject(subId: Int, updatedName: String){
        val values = ContentValues()
        values.put(SUB_NAME,updatedName)
        db?.update(TABLE_NAME2,values,"_id=$subId",null)
    }

    fun updateTeacher(teacherId:Int, updatedName: String,updatedPhone: String, teacherSubject: Int){
        val values = ContentValues()
        values.put(TEACHER_NAME, updatedName)
        values.put(TEACHER_PHONE, updatedPhone)
        values.put(TEACHER_SUB, teacherSubject)
        db?.update(TABLE_NAME4,values, "_id=$teacherId", null )
    }

    fun delClass(clsId: Int){
        db?.delete(TABLE_NAME3, "id=?", arrayOf("$clsId"))
    }

    object AcademyContract {
        object FeedEntry : BaseColumns {
            const val DATABASE_VERSION = 2
            const val DATABASE_NAME = "Academy.db"
            const val TABLE_NAME1 = "StudentTable"
            const val STUD_ID = "_id"
            const val STUD_NAME = "StudentName"
            const val STUD_PHONE = "Phone"
            const val STUD_CITY = "City"
            const val STUD_CLASS = "Class"
            const val TABLE_NAME2 = "SubjectTable"
            const val SUB_ID="_id"
            const val SUB_NAME="SubjectName"
            const val TABLE_NAME3 = "ClassTable"
            const val CLS_ID = "_id"
            const val CLS_NAME = "ClassName"
            const val TABLE_NAME4 = "TeacherTable"
            const val TEACHER_ID="_id"
            const val TEACHER_NAME="TeacherName"
            const val TEACHER_PHONE="TeacherPhone"
            const val TEACHER_SUB="TeacherSubject"
        }
    }

}