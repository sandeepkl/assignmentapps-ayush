package com.example.dbapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dbapp.classes.ClassListActivity
import com.example.dbapp.student.StudentListActivity
import com.example.dbapp.subject.SubjectListActivity
import com.example.dbapp.teacher.TeacherListActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        StdButton?.setOnClickListener {
            val stdIntent = Intent(this, StudentListActivity::class.java)
            startActivity(stdIntent)
        }

        ClsButton.setOnClickListener{
            val clsIntent = Intent(this, ClassListActivity::class.java)
            startActivity(clsIntent)
        }

        SubButton.setOnClickListener {
            val subIntent = Intent(this, SubjectListActivity::class.java)
            startActivity(subIntent)
        }

        TeachButton.setOnClickListener {
            val teachIntent = Intent(this, TeacherListActivity::class.java)
            startActivity(teachIntent)
        }
    }
}
