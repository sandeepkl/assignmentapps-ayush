package com.example.dbapp.classes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import kotlinx.android.synthetic.main.activity_class__new.*

class AddNewClassActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_class__new)
        this.title = "Add New Class"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        Cls_Save.setOnClickListener {
            val name = Cls_Name.text.toString()
            when {
                name.isEmpty() -> {
                    Toast.makeText(this,"Class Name Cannot be Empty..!!",Toast.LENGTH_SHORT).show()
                }
                else -> {
                    dbHandler?.addClass(name)
                    Toast.makeText(this, "Data Added Successfully", Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }

        Cls_Reset.setOnClickListener {
            Cls_Name.setText("")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }
}
