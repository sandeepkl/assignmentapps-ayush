package com.example.dbapp.classes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.classes.ClassListActivity.Companion.CLASS_DATA
import kotlinx.android.synthetic.main.activity_class__edit.*

class UpdateClassActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper?= null
    private var fetchedClassData: ClassModel ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_class__edit)
        this.title = "Update Class"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        if (intent.hasExtra(CLASS_DATA)) {
            fetchedClassData = intent.getParcelableExtra(CLASS_DATA)

            Cls_Name_Edit.setText(fetchedClassData?.name)
        }

        Cls_Update.setOnClickListener {
            val clsId = fetchedClassData!!.id
            val name = Cls_Name_Edit.text.toString()
            when {
                name.isEmpty() -> {
                    Toast.makeText(this,"Class Name Cannot be Empty..!!",Toast.LENGTH_SHORT).show()
                }
                else -> {
                    dbHandler?.updateClass(clsId, name)
                    Toast.makeText(this, "Data Updated Successfully", Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }

        Cls_Cancel.setOnClickListener {
            finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }

}
