package com.example.dbapp.classes

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.RecyclerItemClickListenr
import kotlinx.android.synthetic.main.activity_class__list.*


class ClassListActivity : AppCompatActivity(),ActionMode.Callback{

    companion object {
        const val CLASS_DATA = "class-data"
    }

    private var values: ArrayList<ClassModel> ? = null
    private var dbHandler: AcademyDbHelper?= null
    private var recyclerView: RecyclerView ?= null
    private var adapter: Adapter ? = null
    private var actionMode: ActionMode? = null
    private var isMultiSelect = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_class__list)
        this.title = "Class List"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        recyclerView = findViewById(R.id.ClsView)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)

        values = fetchClasses()
        adapter = Adapter(values ?: ArrayList())
        recyclerView?.adapter = adapter

        recyclerView?.addOnItemTouchListener(RecyclerItemClickListenr(this, recyclerView, object : RecyclerItemClickListenr.OnItemClickListener {

            override fun onItemClick(view: View, position: Int) {
                val clsUpdateIntent = Intent(applicationContext, UpdateClassActivity::class.java)
                clsUpdateIntent.putExtra(CLASS_DATA, values?.get(position))
                startActivityForResult(clsUpdateIntent,1)
                if (isMultiSelect){
                    //multiSelect(position)
                }
            }

            override fun onItemLongClick(view: View?, position: Int) {
                if(!isMultiSelect){
                    values = ArrayList()
                    isMultiSelect = true

                    if (actionMode == null){
                        actionMode = startActionMode(this@ClassListActivity)
                    }
                }
                //multiSelect(position)
            }
        }))

        cNew_button.setOnClickListener {
            val clsNew = Intent(this, AddNewClassActivity::class.java)
            startActivityForResult(clsNew,1)
        }

    }

    /*fun multiSelect(position: Int){
        val data: ClassModel? = values?.get(position)
        if (actionMode != null) {
            if (values!!.contains(data?.id))
//                values[position].id
                values?.remove(Integer.valueOf(data.getId()));
            else
                values?.add(data.getId());

            if (values?.size!! > 0)
                actionMode.setTitle(String.valueOf(values!!.size)); //show selected item count on action mode.
            else{
                actionMode?.title = ""; //remove item count from action mode.
                actionMode?.finish(); //hide action mode.
            }
            adapter.setSelectedIds(values);

        }
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1){
            values = fetchClasses()
            adapter?.notifyListDataChanged(values)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }

    private fun fetchClasses(): ArrayList<ClassModel> {
        val classList = ArrayList<ClassModel>()
        val classCursor = dbHandler?.getAllClass()

        while (classCursor?.moveToNext() == true) {
            val classModel = ClassModel(0)
            classModel.id = classCursor.getInt(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_ID))
            classModel.name = classCursor.getString(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_NAME))
            classList.add(classModel)
        }
        return classList
    }

    class Adapter(private var values:ArrayList<ClassModel>?): RecyclerView.Adapter<Adapter.ViewHolder>() {

        override fun getItemCount() = values?.size?:0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView= LayoutInflater.from(parent.context).inflate(R.layout.class_list_view,parent,false)
            return ViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.textView?.text = values?.get(position)?.name
//            holder.setListeners()

        }

        class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){
            var textView: TextView? = null
            init {
                textView=itemView?.findViewById(R.id.text_item)
            }
        }

        fun notifyListDataChanged(values: ArrayList<ClassModel>?) {
            this.values = values
            notifyDataSetChanged()
        }

    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
//        val inflater: MenuInflater = attr.mode.getMenuInflater()
//        inflater.inflate(R.menu.menu_multi_select, menu)
//        context_menu = menu
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
