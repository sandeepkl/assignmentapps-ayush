package com.example.dbapp.student

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.RecyclerItemClickListenr
import com.example.dbapp.classes.ClassModel
import kotlinx.android.synthetic.main.activity_student__list.*

class StudentListActivity : AppCompatActivity() {

    companion object {
        const val STUDENT_DATA = "student-data"
    }

    private var values: ArrayList<StudentModel> ? = null
    private var dbHandler: AcademyDbHelper?= null
    private var recyclerView: RecyclerView ?= null
    private var adapter: Adapter ? = null
    var classValues: ArrayList<ClassModel> = ArrayList()
    var spin: Spinner? = null
    var selectedClassId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student__list)
        this.title = "Student List"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        recyclerView = findViewById(R.id.StudView)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)



        classValues = fetchClasses()


        spin = findViewById(R.id.spinner_clsName)

        spin?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                selectedClassId = classValues[position].id
                values = fetchStudent(selectedClassId)
                adapter = Adapter(values ?: ArrayList())
                recyclerView?.adapter = adapter
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                values = fetchStudent()
                adapter?.notifyListDataChanged(values)
            }
        }

        val dataAdapter: ArrayAdapter<ClassModel> = ArrayAdapter<ClassModel>(this, android.R.layout.simple_spinner_item, classValues)

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spin?.adapter = dataAdapter


        recyclerView?.addOnItemTouchListener(RecyclerItemClickListenr(this, recyclerView, object : RecyclerItemClickListenr.OnItemClickListener {

            override fun onItemClick(view: View, position: Int) {
                val studentUpdateIntent = Intent(applicationContext, UpdateStudentActivity::class.java)
                studentUpdateIntent.putExtra(STUDENT_DATA, values?.get(position))
                startActivityForResult(studentUpdateIntent,1)
            }

            override fun onItemLongClick(view: View?, position: Int) {

            }
        }))


        sNew_button.setOnClickListener {
            val tNew = Intent(this, AddNewStudentActivity::class.java)
            startActivityForResult(tNew,1)
        }
    }

    private fun fetchClasses(): ArrayList<ClassModel> {
        val classList = ArrayList<ClassModel>()
        val classCursor = dbHandler?.getAllClass()

        while (classCursor?.moveToNext() == true) {
            val classModel = ClassModel(0)
            classModel.id = classCursor.getInt(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_ID))
            classModel.name = classCursor.getString(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_NAME))

            classList.add(classModel)
        }

        return classList
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1){
            values = fetchStudent(selectedClassId)
            adapter?.notifyListDataChanged(values)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }

    private fun fetchStudent(clsId: Int): ArrayList<StudentModel> {
        val StudentList = ArrayList<StudentModel>()
        val studentCursor = dbHandler?.getAllStudent(clsId)

        while (studentCursor?.moveToNext() == true) {
            val studentModel = StudentModel(0)
            studentModel.id = studentCursor.getInt(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_ID))
            studentModel.name = studentCursor.getString(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_NAME))
            studentModel.phone = studentCursor.getString(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_PHONE))
            studentModel.city = studentCursor.getString(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_CITY))
            studentModel.clsId = studentCursor.getInt(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_CLASS))
            StudentList.add(studentModel)
        }
        return StudentList
    }

    private fun fetchStudent(): ArrayList<StudentModel> {
        val StudentList = ArrayList<StudentModel>()
        val studentCursor = dbHandler?.getAllStudent()

        while (studentCursor?.moveToNext() == true) {
            val studentModel = StudentModel(0)
            studentModel.id = studentCursor.getInt(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_ID))
            studentModel.name = studentCursor.getString(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_NAME))
            studentModel.phone = studentCursor.getString(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_PHONE))
            studentModel.city = studentCursor.getString(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_CITY))
            studentModel.clsId = studentCursor.getInt(studentCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.STUD_CLASS))
            StudentList.add(studentModel)
        }

        return StudentList
    }

    class Adapter(private var values:ArrayList<StudentModel>?): RecyclerView.Adapter<Adapter.ViewHolder>() {

        override fun getItemCount()= values?.size?:0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView= LayoutInflater.from(parent.context).inflate(R.layout.student_list_view,parent,false)
            return ViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.textView?.text = values?.get(position)?.name
        }

        class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){
            var textView: TextView? = null
            init {
                textView=itemView?.findViewById(R.id.text_item)
            }
        }

        fun notifyListDataChanged(values: ArrayList<StudentModel>?) {
            this.values = values
            notifyDataSetChanged()
        }
    }
}