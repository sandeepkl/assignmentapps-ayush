package com.example.dbapp.student

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.classes.ClassModel
import com.example.dbapp.student.StudentListActivity.Companion.STUDENT_DATA
import kotlinx.android.synthetic.main.activity_student__edit.*

class UpdateStudentActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper ?= null
    private var fetchedStudentData: StudentModel ?= null
    var values: ArrayList<ClassModel> = ArrayList()
    var spin: Spinner? = null
    var selectedClassId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student__edit)
        this.title = "Update Student"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        values = fetchClasses()

        spin = findViewById(R.id.Class_Id_Edit)

        val dataAdapter: ArrayAdapter<ClassModel> = ArrayAdapter<ClassModel>(this, android.R.layout.simple_spinner_item, values)

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spin?.adapter = dataAdapter

        spin?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                selectedClassId = values[position].id
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        }

        if (intent.hasExtra(STUDENT_DATA)) {
            fetchedStudentData = intent?.extras?.getParcelable(STUDENT_DATA)

            Stud_Name_Edit.setText(fetchedStudentData?.name)
            Stud_Phone_Edit.setText(fetchedStudentData?.phone)
            Stud_City_Edit.setText(fetchedStudentData?.city)

            for (i in 0 until values.size) {
                if (values[i].id == fetchedStudentData?.clsId) {
                    Class_Id_Edit.setSelection(i)
                    break
                }
            }

        }

        Stud_Update.setOnClickListener {
            val id = fetchedStudentData!!.id
            val name = Stud_Name_Edit.text.toString()
            val phone = Stud_Phone_Edit.text.toString()
            val city = Stud_City_Edit.text.toString()

            when {
                name.isEmpty() -> {
                    Toast.makeText(this, "Name Cannot be Empty...!!", Toast.LENGTH_SHORT).show();
                }
                city.isEmpty() -> {
                    Toast.makeText(this, "City Cannot be Empty...!!", Toast.LENGTH_SHORT).show();
                }
                else -> {
                    when {
                        phone.isEmpty() -> {
                            Toast.makeText(this, "Phone Cannot be Empty...!!", Toast.LENGTH_SHORT)
                                .show();
                        }
                        phone.length != 10 -> {
                            Toast.makeText(this, "Enter Valid Phone...!!", Toast.LENGTH_SHORT)
                                .show();
                        }
                        else -> {
                            dbHandler?.updateStudent(id, name, phone, city, selectedClassId)
                            Toast.makeText(this, "Data Updated Successfully", Toast.LENGTH_LONG)
                                .show()
                            finish()
                        }
                    }
                }
            }
        }
        Stud_Cancel.setOnClickListener {
            finish()
        }

    }

    private fun fetchClasses(): ArrayList<ClassModel> {
        val classList = ArrayList<ClassModel>()
        val classCursor = dbHandler?.getAllClass()
        while (classCursor?.moveToNext() == true) {
            val classModel = ClassModel(0)
            classModel.id = classCursor.getInt(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_ID))
            classModel.name = classCursor.getString(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_NAME))
            classList.add(classModel)
        }
        return classList
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }
}
