package com.example.dbapp.student

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class StudentModel(var id: Int, var name: String? = null, var phone: String? = null,
                   var city: String? = null, var clsId: Int = 0
) : Parcelable {

    override fun toString(): String {
        return name?:""
    }

}