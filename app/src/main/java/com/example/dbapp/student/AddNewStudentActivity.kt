package com.example.dbapp.student

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.classes.ClassModel
import kotlinx.android.synthetic.main.activity_student_new.*


class AddNewStudentActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper?= null
    var values: ArrayList<ClassModel> = ArrayList()
    var spin: Spinner? = null
    var selectedClassId = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_new)
        this.title = "Add New Student"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        values = fetchClasses()

        spin = findViewById(R.id.spinner_Class_ID)

        spin?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                selectedClassId = values[position].id
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        }

        // Creating adapter for spinner
        val dataAdapter: ArrayAdapter<ClassModel> = ArrayAdapter<ClassModel>(this, android.R.layout.simple_spinner_item, values)

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spin?.adapter = dataAdapter

        Stud_Save.setOnClickListener {
            val name = Stud_Name.text.toString()
            val phone = Stud_Phone.text.toString()
            val city = Stud_City.text.toString()

            when {
                name.isEmpty() -> {
                    Toast.makeText(this, "Name Cannot be Empty...!!", Toast.LENGTH_SHORT).show()
                }
                city.isEmpty() -> {
                    Toast.makeText(this, "City Cannot be Empty...!!", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    if (phone.isEmpty()) {
                        Toast.makeText(this, "Phone Cannot be Empty...!!", Toast.LENGTH_SHORT).show()
                    } else if (phone.length!=10) {
                        Toast.makeText(this, "Enter Valid Phone...!!", Toast.LENGTH_SHORT).show()
                    } else {
                        dbHandler?.addStudent(name, phone, city, selectedClassId)
                        Toast.makeText(this, "Data Added Successfully", Toast.LENGTH_LONG).show()
                        finish()
                    }
                }
            }
        }

        Stud_Reset.setOnClickListener {
            Stud_Name.setText("")
            Stud_Phone.setText("")
            Stud_City.setText("")
        }
    }

    private fun fetchClasses(): ArrayList<ClassModel> {
        val classList = ArrayList<ClassModel>()
        val classCursor = dbHandler?.getAllClass()
        while (classCursor?.moveToNext() == true) {
            val classModel = ClassModel(0)
            classModel.id = classCursor.getInt(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_ID))
            classModel.name = classCursor.getString(classCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.CLS_NAME))
            classList.add(classModel)
        }
        return classList
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }
}