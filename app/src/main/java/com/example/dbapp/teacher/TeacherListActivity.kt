package com.example.dbapp.teacher

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.RecyclerItemClickListenr
import com.example.dbapp.subject.SubjectModel
import kotlinx.android.synthetic.main.activity_teacher__list.*

class TeacherListActivity : AppCompatActivity() {

    companion object {
        const val TEACHER_DATA = "teacher-data"
    }

    private var values: ArrayList<TeacherModel> ? = null
    private var dbHandler: AcademyDbHelper?= null
    private var recyclerView: RecyclerView ?= null
    private var adapter: Adapter ? = null
    var subValues: ArrayList<SubjectModel> = ArrayList()
    var spin: Spinner? = null
    var selectedSubId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher__list)
        this.title = "Teacher List"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        recyclerView = findViewById(R.id.TeacherView)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)


        subValues = fetchSubject()

        spin = findViewById(R.id.spinner_subName)

        spin?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                selectedSubId = subValues[position].id
                values = fetchTeacher(selectedSubId)
                adapter = Adapter(values ?: ArrayList())
                recyclerView?.adapter = adapter
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                values = fetchTeacher()
                adapter?.notifyListDataChanged(values)
            }
        }

        val dataAdapter: ArrayAdapter<SubjectModel> = ArrayAdapter<SubjectModel>(this, android.R.layout.simple_spinner_item, subValues)

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spin?.adapter = dataAdapter


        recyclerView?.addOnItemTouchListener(RecyclerItemClickListenr(this, recyclerView, object : RecyclerItemClickListenr.OnItemClickListener {

            override fun onItemClick(view: View, position: Int) {
                val teacherUpdateIntent = Intent(applicationContext, UpdateTeacherActivity::class.java)
                teacherUpdateIntent.putExtra(TEACHER_DATA, values?.get(position))
                startActivityForResult(teacherUpdateIntent,1)
            }

            override fun onItemLongClick(view: View?, position: Int) {

            }
        }))


        tNew_button.setOnClickListener {
            val tNew = Intent(this, AddNewTeacherActivity::class.java)
            startActivityForResult(tNew,1)
        }
    }

    private fun fetchSubject(): ArrayList<SubjectModel> {
        val subList = ArrayList<SubjectModel>()
        val subCursor = dbHandler?.getAllSubject()

        while (subCursor?.moveToNext() == true) {
            val subjectModel = SubjectModel(0)
            subjectModel.id = subCursor.getInt(subCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_ID))
            subjectModel.name = subCursor.getString(subCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_NAME))

            subList.add(subjectModel)
        }

        return subList
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1){
            values = fetchTeacher(selectedSubId)
            adapter?.notifyListDataChanged(values)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }

    private fun fetchTeacher(subId: Int): ArrayList<TeacherModel> {
        val teacherList = ArrayList<TeacherModel>()
        val teacherCursor = dbHandler?.getAllTeacher(subId)

        while (teacherCursor?.moveToNext() == true) {
            val teacherModel = TeacherModel(0)
            teacherModel.id = teacherCursor.getInt(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_ID))
            teacherModel.name = teacherCursor.getString(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_NAME))
            teacherModel.phone = teacherCursor.getString(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_PHONE))
            teacherModel.subId = teacherCursor.getInt(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_SUB))
            teacherList.add(teacherModel)
        }
        return teacherList
    }

    private fun fetchTeacher(): ArrayList<TeacherModel> {
        val teacherList = ArrayList<TeacherModel>()
        val teacherCursor = dbHandler?.getAllTeacher()

        while (teacherCursor?.moveToNext() == true) {
            val teacherModel = TeacherModel(0)
            teacherModel.id = teacherCursor.getInt(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_ID))
            teacherModel.name = teacherCursor.getString(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_NAME))
            teacherModel.phone = teacherCursor.getString(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_PHONE))
            teacherModel.subId = teacherCursor.getInt(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_SUB))
            teacherList.add(teacherModel)
        }

        return teacherList
    }

    class Adapter(private var values:ArrayList<TeacherModel>?): RecyclerView.Adapter<Adapter.ViewHolder>() {

        override fun getItemCount()= values?.size?:0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView= LayoutInflater.from(parent.context).inflate(R.layout.teacher_list_view,parent,false)
            return ViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.textView?.text = values?.get(position)?.name
        }

        class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){
            var textView: TextView? = null
            init {
                textView=itemView?.findViewById(R.id.text_item)
            }
        }

        fun notifyListDataChanged(values: ArrayList<TeacherModel>?) {
            this.values = values
            notifyDataSetChanged()
        }
    }
}