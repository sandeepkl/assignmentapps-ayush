package com.example.dbapp.teacher

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.subject.SubjectModel
import kotlinx.android.synthetic.main.activity_teacher__new.*

class AddNewTeacherActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper?= null
    var values: ArrayList<SubjectModel> = ArrayList()
    var selectedSubjectId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher__new)
        this.title = "Add New Teacher"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        values = fetchSubject()

        val spin: Spinner = findViewById(R.id.Subject_id)

        spin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                selectedSubjectId = values[position].id
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        }

        // Creating adapter for spinner
        val dataAdapter: ArrayAdapter<SubjectModel> =
            ArrayAdapter<SubjectModel>(this, android.R.layout.simple_spinner_item, values)

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        spin.adapter = dataAdapter

        Teacher_Save.setOnClickListener {
            val name = Teacher_Name.text.toString()
            val phone = Teacher_Phone.text.toString()

            if (name.isEmpty()) {
                Toast.makeText(this, "Name Cannot be Empty...!!", Toast.LENGTH_SHORT).show();
            }
            else {
                when {
                    phone.isEmpty() -> {
                        Toast.makeText(this, "Phone Cannot be Empty...!!", Toast.LENGTH_SHORT).show();
                    }
                    phone.length!=10 -> {
                        Toast.makeText(this, "Enter Valid Phone...!!", Toast.LENGTH_SHORT).show();
                    }
                    else -> {
                        dbHandler?.addTeacher(name, phone,selectedSubjectId)
                        Toast.makeText(this, "Data Added Successfully", Toast.LENGTH_LONG).show()
                        finish()
                    }
                }
            }
        }

        Teacher_Reset.setOnClickListener {
            Teacher_Name.setText("")
            Teacher_Phone.setText("")
        }
    }

    private fun fetchSubject(): ArrayList<SubjectModel> {
        val subList = ArrayList<SubjectModel>()
        val subCursor = dbHandler?.getAllSubject()

        while (subCursor?.moveToNext() == true) {
            val subjectModel = SubjectModel(0)
            subjectModel.id = subCursor.getInt(subCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_ID))
            subjectModel.name = subCursor.getString(subCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_NAME))

            subList.add(subjectModel)
        }

        return subList
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }
}
