package com.example.dbapp.teacher

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.subject.SubjectModel
import com.example.dbapp.teacher.TeacherListActivity.Companion.TEACHER_DATA
import kotlinx.android.synthetic.main.activity_teacher__edit.*

class UpdateTeacherActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper ?= null
    private var fetchedTeacherData: TeacherModel ?= null
    var values: ArrayList<SubjectModel> = ArrayList()
    var spin: Spinner? = null
    var selectedSubjectId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher__edit)
        this.title = "Update Teacher"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        values = fetchSubject()

        spin = findViewById(R.id.Subject_id_Edit)

        val dataAdapter: ArrayAdapter<SubjectModel> = ArrayAdapter<SubjectModel>(this, android.R.layout.simple_spinner_item, values)

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spin?.adapter = dataAdapter

        spin?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                selectedSubjectId = values[position].id
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        }

        if (intent.hasExtra(TEACHER_DATA)) {
            fetchedTeacherData = intent?.extras?.getParcelable(TEACHER_DATA)

            Teacher_Name_Edit.setText(fetchedTeacherData?.name)
            Teacher_Phone_Edit.setText(fetchedTeacherData?.phone)

            for (i in 0 until values.size) {
                if (values[i].id == fetchedTeacherData?.subId) {
                    Subject_id_Edit.setSelection(i)
                    break
                }
            }

        }

        Teacher_Update.setOnClickListener {
            val id = fetchedTeacherData!!.id
            val name = Teacher_Name_Edit.text.toString()
            val phone = Teacher_Phone_Edit.text.toString()


            when {
                name.isEmpty() -> {
                    Toast.makeText(this, "Name Cannot be Empty...!!", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    when {
                        phone.isEmpty() -> {
                            Toast.makeText(this, "Phone Cannot be Empty...!!", Toast.LENGTH_SHORT).show()
                        }
                        phone.length != 10 -> {
                            Toast.makeText(this, "Enter Valid Phone...!!", Toast.LENGTH_SHORT).show()
                        }
                        else -> {
                            dbHandler?.updateTeacher(id, name, phone, selectedSubjectId)
                            Toast.makeText(this, "Data Updated Successfully", Toast.LENGTH_LONG).show()
                            finish()
                        }
                    }
                }
            }
        }
        Teacher_Cancel.setOnClickListener {
            finish()
        }

    }

    private fun fetchSubject(): ArrayList<SubjectModel> {
        val subjectList = ArrayList<SubjectModel>()
        val subjectCursor = dbHandler?.getAllSubject()
        while (subjectCursor?.moveToNext() == true) {
            val subModel = SubjectModel(0)
            subModel.id = subjectCursor.getInt(subjectCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_ID))
            subModel.name = subjectCursor.getString(subjectCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_NAME))
            subjectList.add(subModel)
        }
        return subjectList
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }
}