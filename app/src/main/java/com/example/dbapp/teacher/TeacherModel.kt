package com.example.dbapp.teacher


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class TeacherModel(var id: Int, var name: String? = null, var phone: String? = null, var subId: Int? = 0
) : Parcelable {

    override fun toString(): String {
        return name?:""
    }

}