package com.example.dbapp.subject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.teacher.TeacherModel
import kotlinx.android.synthetic.main.activity_subject__new.*

class AddNewSubjectActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper?= null
    var values: ArrayList<TeacherModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subject__new)
        this.title = "Add New Subject"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        Sub_Save.setOnClickListener {
            val name = Sub_Name.text.toString()
            when {
                name.isEmpty() -> {
                    Toast.makeText(this, "Subject Name Cannot be Empty...!!", Toast.LENGTH_SHORT).show();
                }
                else -> {
                    dbHandler?.addSubject(name)
                    Toast.makeText(this, "Data Added Successfully", Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }

        Sub_Reset.setOnClickListener {
            Sub_Name.setText("")
        }

//        values = fetchTeacher()
//
//        val spin: Spinner = findViewById(R.id.spinner_teacher_Name)
//
//        // Creating adapter for spinner
//        val dataAdapter: ArrayAdapter<TeacherModel> = ArrayAdapter<TeacherModel>(this, android.R.layout.simple_spinner_item, values)
//
//        // Drop down layout style - list view with radio button
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//
//        // attaching data adapter to spinner
//        spin.adapter = dataAdapter

    }

//    private fun fetchTeacher(): ArrayList<TeacherModel> {
//        val teacherList = ArrayList<TeacherModel>()
//        val teacherCursor = dbHandler?.getAllTeacher()
//
//        while (teacherCursor?.moveToNext() == true) {
//            val teacherModel = TeacherModel()
//            teacherModel.name = teacherCursor.getString(teacherCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.TEACHER_NAME))
//            teacherList.add(teacherModel)
//        }
//        return teacherList
//    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }
}
