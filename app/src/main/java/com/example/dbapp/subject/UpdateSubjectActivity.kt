package com.example.dbapp.subject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.subject.SubjectListActivity.Companion.SUBJECT_DATA
import kotlinx.android.synthetic.main.activity_subject__edit.*

class UpdateSubjectActivity : AppCompatActivity() {

    private var dbHandler: AcademyDbHelper?= null
    private var fetchedSubjectData: SubjectModel ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subject__edit)
        this.title = "Update Subject"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        if (intent.hasExtra(SUBJECT_DATA)) {
            fetchedSubjectData = intent.getParcelableExtra(SUBJECT_DATA)

            Sub_Name_Edit.setText(fetchedSubjectData?.name)
        }

        Sub_Update.setOnClickListener {
            val subId = fetchedSubjectData!!.id
            val name = Sub_Name_Edit.text.toString()
            when {
                name.isEmpty() -> {
                    Toast.makeText(this,"Subject Name Cannot be Empty..!!",Toast.LENGTH_SHORT).show()
                }
                else -> {
                    dbHandler?.updateSubject(subId, name)
                    Toast.makeText(this, "Data Updated Successfully", Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }

        Sub_Cancel.setOnClickListener {
            finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }

}
