package com.example.dbapp.subject

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class SubjectModel(var id: Int, var name: String? = null) : Parcelable {

    override fun toString(): String {
        return name?:""
    }

    override fun equals(other: Any?): Boolean {
        if (other is SubjectModel) {
            return other.id == id
        }
        return false
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + (name?.hashCode() ?: 0)
        return result
    }


}
