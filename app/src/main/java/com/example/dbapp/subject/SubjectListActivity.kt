package com.example.dbapp.subject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dbapp.AcademyDbHelper
import com.example.dbapp.R
import com.example.dbapp.RecyclerItemClickListenr
import kotlinx.android.synthetic.main.activity_subject__list.*

class SubjectListActivity : AppCompatActivity() {

    companion object {
        const val SUBJECT_DATA = "subject-data"
    }

    private var values: ArrayList<SubjectModel> ? = null
    private var dbHandler: AcademyDbHelper?= null
    private var recyclerView: RecyclerView ?= null
    private var adapter: Adapter ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subject__list)
        this.title = "Subject List"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        dbHandler = AcademyDbHelper(this)
        dbHandler?.openDB()

        recyclerView = findViewById(R.id.SubView)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)

        values = fetchSubject()
        adapter = Adapter(values ?: ArrayList())
        recyclerView?.adapter = adapter

        recyclerView?.addOnItemTouchListener(
            RecyclerItemClickListenr(this, recyclerView, object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val subjectUpdateIntent = Intent(applicationContext, UpdateSubjectActivity::class.java)
                        subjectUpdateIntent.putExtra(SUBJECT_DATA, values?.get(position))
                        startActivityForResult(subjectUpdateIntent,1)
                    }
                    override fun onItemLongClick(view: View?, position: Int) {

                    }
                })
        )

        subNew_button.setOnClickListener {
            val subNew = Intent(this, AddNewSubjectActivity::class.java)
            startActivityForResult(subNew,1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1){
            values = fetchSubject()
            adapter?.notifyListDataChanged(values)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        dbHandler?.closeDB()
        super.onBackPressed()
    }

    private fun fetchSubject(): ArrayList<SubjectModel> {
        val subList = ArrayList<SubjectModel>()
        val subCursor = dbHandler?.getAllSubject()

        while (subCursor?.moveToNext() == true) {
            val subjectModel = SubjectModel(0)
            subjectModel.id = subCursor.getInt(subCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_ID))
            subjectModel.name = subCursor.getString(subCursor.getColumnIndex(AcademyDbHelper.AcademyContract.FeedEntry.SUB_NAME))

            subList.add(subjectModel)
        }

        return subList
    }

    class Adapter(private var values:ArrayList<SubjectModel>?): RecyclerView.Adapter<Adapter.ViewHolder>() {

        override fun getItemCount()= values?.size?:0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView= LayoutInflater.from(parent.context).inflate(R.layout.subject_list_view,parent,false)
            return ViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.textView?.text = values?.get(position)?.name
        }

        class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){
            var textView: TextView? = null
            init {
                textView=itemView?.findViewById(R.id.text_item)
            }
        }

        fun notifyListDataChanged(values: ArrayList<SubjectModel>?) {
            this.values = values
            notifyDataSetChanged()
        }
    }
}
